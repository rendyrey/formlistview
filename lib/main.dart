import 'package:flutter/material.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Form and Grid view',
        theme: ThemeData(
            primaryColor: Colors.green
        ),
        home: HomeScreen()
    );
  }
}

class HomeScreen extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text('Form and Grid View')),
        body: Column(
          // crossAxisCount: 1,
          children: <Widget>[
            Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  // crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: "Masukkan Namamu",
                        labelText: "Nama",
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.redAccent, width: 0.0),
                        ),
                      ),
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: "Masukkan Umur",
                        labelText: "Umur",
                      ),
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter some text';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                        hintText: "Masukkan Hobi",
                        labelText: "Hobi",
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 16.0),
                      child: RaisedButton(
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            Scaffold.of(context)
                                .showSnackBar(
                                SnackBar(content: Text('Processing Data')));
                          }
                        },
                        child: Text('Submit'),
                      ),
                    )
                  ],
                )
            ),
            new Container(
              height: 250.0,
              child: new ListView(
                scrollDirection: Axis.vertical,
                children: <Widget>[
                  new RaisedButton(
                    onPressed: null,
                    child: new Text("A"),
                  ),
                  new RaisedButton(
                    onPressed: null,
                    child: new Text("B"),
                  ),
                  new RaisedButton(
                    onPressed: null,
                    child: new Text("C"),
                  ),
                  new RaisedButton(
                    onPressed: null,
                    child: new Text("D"),
                  ),
                  new RaisedButton(
                    onPressed: null,
                    child: new Text("E"),
                  ),
                  new RaisedButton(
                    onPressed: null,
                    child: new Text("F"),
                  )
                ],
              ),
            )
          ],
        )

    );
  }
}

